def mayor(num1, num2):
        resultado = num1
        if num1 < num2:
            resultado = num2
        
        return resultado
        
        
#(###########)
#(###main####)
#(###########)
        
if __name__ == '__main__':
    lista = [1,2,3,4]
    print(mayor(lista[0], lista[1]))
    print(mayor(lista[2], lista[3]))
    print(mayor(mayor(lista[0], lista[1]), mayor(lista[2], lista[3])))